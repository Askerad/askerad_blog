---
layout: ../../layouts/Article.astro
title: "Pourquoi écrire un blog avec Astro, Vue et Tailwind"
author: "Charles Stieffenhofer"
date: "09 Août 2022"
image: "https://picsum.photos/1500/500"
tags: 
    - Vue 
    - Astro 
    - Tailwind 
---

Dans un monde où les librairies JS front-end pullulent et où cinquantes standards différents éxistent pour faire un site web, c'est quand même fou que la **Search Engine Optimisation** reste un problème majeur et une contrainte bloquante dans le design des projets webs.

Des librairies ultra populaires tels que **Vue** ou **React** se heurtent a des contraintes insurmontables dés que l'on souhaite les utiliser dans des projets qui cherchent a être visible, et a respecter les standards imposé par notre maître a tous Google.

En effet, quand le moteur de recherche principal que tout le monde utilise refuse d'éxécuter le Javascript de la page, c'est un poil embetant. Surtout quand on code une application web one-page qui requiert JS pour ne serait-ce que gérer le changement de page.

## Et bah, on a qu'a éxécuter le Javascript avant alors !

## Astro, ou comment dire "Merde" a tous les standards actuels

## Vue c'est quand meme vachement bien.

C'est une page générée depuis un fichier markdown !